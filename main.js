const Timer = {
    audio: new Audio('asset/Ding.mp3'),

    bar: document.getElementById('bar'),

    label: document.getElementById('label'),

    pauseDuration: 0,

    sessionDuration: 0,

    start: async function() {
        const me = this,
            sessionCount = parseInt(document.getElementById('sessionCount').value);

        me.pauseDuration = parseInt(document.getElementById('pause').value) * 1000,
        me.sessionDuration = parseInt(document.getElementById('sessionLength').value) * 1000,

        await me.pause('Starting soon');

        for (let i = 0; i < sessionCount; i++) {
            await me.session('Session ' + (i+1));
            
            await me.pause();
        }
    },

    pause: function(message) {
        var me = this,
            msg = message || 'Pause';
    
        me.label.innerHTML = msg;
    
        me.bar.setAttribute('style', 'width: 0; transition-duration: 0ms;')

        return new Promise(resolve => {
            setTimeout(() => {
                me.audio.play();
                resolve();
            }, me.pauseDuration)
        });
    },
    
    session: function(message) {
        var me = this,
            msg = message || 'Session',
            sessionDuration = me.sessionDuration;
    
        me.label.innerHTML = msg;
    
        me.bar.setAttribute('style', 'width: 100%; transition-duration: ' + sessionDuration + 'ms;')

        return new Promise(resolve => {
            setTimeout(() => {
                me.audio.play();
                resolve();
            }, sessionDuration)
        });
    }
}